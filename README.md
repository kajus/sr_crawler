<!-- omit in TOC -->

# sr_crawler
============

> ** command line crawler plus scraper to extract data from webpages written in [Rust](https://www.rust-lang.org/)**


1. [About](#about)
2. [Installation](https://github.com)
2. [Examples](https)
4. [CHANGELOG](https)
6. [TODO](https:)
7. [Questions & Discussions](https)
8. [Contributing](https)


About
-------
A tool to extract email addresses from a website.


Install from source
-------------------

Prerequisites:

- [Rust](https://www.rust-lang.org/tools/install)
- [git](https://git-scm.com/download/linux)

Clone the repository:

> git clone git@gitlab.com:kajus/sr_crawler.git

Change into the cloned directory:

> cd sr_crawler

Build the program:

> cargo build 

Change into the target folder:

> cd target/debug

Execute the program:

> ./sr_crawler


Examples
---------
To crawl and scrap email adresses from a website, just execute the program and provide the page as argument:

> ./sr_crawler https://www.bumbar.blog

You can show all options the program provides with '--help':

> ./sr_crawler --help 

If you need faster results, you can increase the concurrency with '--concurrency <number>'.
Default here is 4, the higher the number the faster the crawler, but also a higher risk to get blocked by
the server for suspicious behavior.

> ./sr_crawler https://www.bumbar.blog -c 20 

You can tell the crawler with '--robots-txt' if the page has a robots.txt.

> ./sr_crawler https://www.bumbar.blog -r 

If you already know what page contains the email adresses you wanna scrap, you can do that by 
providing '--single-page' as parameter.

> .sr_crawler https://www.bumbar.blog -s 

CHANGELOG
---------

TODO
----

- [x] Working MVP
- [x] Unit testing 
- [x] Multithreaded crawler 
- [ ] Multithreaded scraper 
- [ ] Refactor README.md
- [ ] Provide executables instead of self building 
- [ ] Add function to scrap for universities 
- [ ] Add database/backend access 
- [ ] Api 
- [ ] Option to read urls from a file 


Questions & Discussions
-----------------------


Contributing
------------

