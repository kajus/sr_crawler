use sr_crawler::cli::Cli;
use sr_crawler::domain::Domain;
use std::time::Instant;
use sr_crawler::mail_scraper::MailScraper;
use itertools::Itertools;
use csv::Writer;
use std::error::Error;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    //Instantiate
    let mut writer = Writer::from_path("test.csv")?;
    let now = Instant::now();
    let cli = Cli::new();
    let mut website: Domain = Domain::new(&cli.get_url());
    let mut scraper = MailScraper::new(cli.get_verbose(), cli.get_concurrency());

    //Configure 
    website.configuration.verbose = cli.get_verbose(); // Defaults to false
    website.configuration.respect_robots_txt = cli.get_robots_txt();
    website.configuration.concurrency = cli.get_concurrency(); // Defaults to 4

    //Start scrapping according to configuration
    match cli.get_single_page_flag(){
        true => {
            println!("[Debug] -> flag is true: {}", cli.get_single_page_flag());
            if let Err(err) = scraper.scrap_mails(&cli.get_url()).await{
                println!("{:?}", err);
           }
        }
        false => {
            website.crawl(); 
            for page in website.get_pages() {
                println!("-> scrap: {}", page.get_url());
                if let Err(e) = scraper.scrap_mails(&page.get_url()).await{
                    println!("{:?}", e)
                }
            }
        }
    }

    //get the mails remove duplicates and write to csv
    let mails = scraper.get_mail_address();
    let filterted_mails = mails.iter().unique_by(|a| &a.email_adresses).collect::<Vec<_>>();
    for element in filterted_mails{
        println!("mails scrapped: {:?}", element);
        writer.write_record(&[element.email_adresses.clone(), element.url.clone(), element.timestamp.to_string().clone()])?;
    }
    println!("Elapsed: {:.2?}", now.elapsed());
    writer.flush()?;
    Ok(())}

