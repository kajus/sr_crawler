use std::ffi::OsString;
use clap::Parser;

#[derive(Parser, Debug)]
#[clap(about, version, author)]
pub struct Cli {
    ///Url of the website we wanna crawl
    url: String,

    #[clap(short, long)]
    ///Runs the program in verbose mode
    verbose: bool,

    #[clap(short, long)]
    ///Dissables crawling and only scraps that provided website
    single_page: bool,
    
    #[clap(short, long)]
    ///Enables robot.txt
    robots_txt: bool,

    #[clap(short, long, default_value_t = 4)]
    ///Simultaneously
    concurrency: usize,
}

impl Default for Cli {
    fn default() -> Self {
        Self::new()
    }
}

impl Cli {
    pub fn new() -> Self {
        Self::new_from(std::env::args_os()).unwrap_or_else(|e| e.exit())
    }
    fn new_from<I, T>(_args: I) -> Result<Self, clap::Error>
    where
        I: Iterator<Item = T>,
        T: Into<OsString> + Clone,
    {
        let cli = Cli::parse();

        Ok(Cli {
            url: cli.url.to_string(),
            verbose: cli.verbose,
            single_page: cli.single_page,
            robots_txt: cli.robots_txt,
            concurrency: cli.concurrency,
        })
    }

    pub fn get_single_page_flag(&self) -> bool { 
        self.single_page
    }

    pub fn get_url(&self) -> String {
        let url_option = &self.url;
        url_option.to_string()
    }
    pub fn get_verbose(&self) -> bool {
        self.verbose
    }

    pub fn get_concurrency(&self) -> usize {
        self.concurrency
    }

    pub fn get_robots_txt(&self) -> bool {
        self.robots_txt
    }

    pub fn print_struct(&self){
        println!("url: {0}
                 \nverbose_mode: {1}
                 \nrobots_txt: {2}
                 \nsingle_page: {3}
                 \nconcurrency: {4:?}", 
                 self.url, 
                 self.verbose, 
                 self.robots_txt,
                 self.single_page,
                 self.concurrency
                 )
    }
}
