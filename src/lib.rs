extern crate rayon;
extern crate reqwest;
extern crate robotparser;
extern crate scraper;
extern crate url;
extern crate clap;

// config for webpage 
pub mod config;
// Site scrapper
pub mod webpage;
// Website to crawl
pub mod domain;
// Cli usage;
pub mod cli;
// Helper functions
pub mod utils;
// MailScraper
pub mod mail_scraper;
// CsvModel
pub mod csv_model;
