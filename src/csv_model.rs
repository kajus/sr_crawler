
use serde::{Deserialize, Serialize};


use polars::prelude::*;

#[derive(Debug, Deserialize, Serialize)]
pub struct DataFrame {
    //header: csv::StringRecord,
    email: Vec<String>,
    url: Vec<String>,
    //timestamp: Vec<DateTime<Utc>>,
}

impl DataFrame {
    fn new() -> DataFrame {
        DataFrame {
            //header: csv::StringRecord::new(),
            email: Vec::new(),
            url: Vec::new(),
            //timestamp: Vec::new(),
        }
    }

   // fn write_csv(&self, out_file_path: &str, rdr: HashSet<MailDAO>)  {
   //     let mut wtr = csv::WriterBuilder::new().from_path(out_file_path);

   //     let mut df = rdr.clone();
   //     for result in df.iter() {
   //         wtr.write_record(result);
   //     }

   //     wtr.flush();
   // }

    fn read_csv(filepath: &str, has_headers: bool) -> DataFrame {
        // Open file
        let file = std::fs::File::open(filepath).unwrap();
        let mut rdr = csv::ReaderBuilder::new()
            .has_headers(has_headers)
            .from_reader(file);

        let mut data_frame = DataFrame::new();

        // push all the records
        for result in rdr.records() {
            let record = result.unwrap();
            data_frame.push(&record);
        }
        data_frame
    }

    fn push(&mut self, row: &csv::StringRecord) {
        // get name
        self.email.push(row[0].to_string());
        // get datetime
        self.url.push(row[1].to_string());
        // get speed
        //self.timestamp.push(row[2].parse().unwrap());
    }
}
