extern crate select;
extern crate reqwest;
use soup::prelude::*;
use regex::Regex;
use std::collections::HashSet;
use chrono::prelude::*;

#[derive(Debug)]
pub struct MailScraper {
    mail_dao: HashSet<MailDAO>,
    verbose: bool,
    concurrency: usize,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct MailDAO {
    pub email_adresses: String,
    pub url: String,
    pub timestamp: DateTime<Utc>,
}

impl MailDAO{
    fn new(mail_adresses: &str, url: &str, timestp: DateTime<Utc>) -> Self{
        Self{
            email_adresses: mail_adresses.to_string(), 
            url: url.to_string(), 
            timestamp: timestp,
        }
    }
}

impl MailScraper {
    pub fn new(verb: bool, conc: usize) -> Self {
        Self {
            mail_dao: HashSet::new(),
            verbose: verb,
            concurrency: conc,
        }
    }

    pub async fn scrap_mails(&mut self, url_link: &str) ->   Result<(), Box<dyn std::error::Error>>{
        let res = reqwest::get(url_link).await?.text().await?;
        let soup = Soup::new(&res);

        for (_i, link) in soup.tag(true).find_all().enumerate() {
            if self.mail_validation(link.text()){
                self.add_adresses(&link.text(), url_link, Utc::now());
            };
        }
        if self.verbose{
            println!("matched_mail: {0:?}", self.mail_dao);
        }
        Ok(())
    }

    fn add_adresses(&mut self, mail: &str, url: &str, timestamp: DateTime<Utc>) {
        self.mail_dao.insert(MailDAO::new(mail, url, timestamp));
    }

    pub fn get_mail_address(&self) -> HashSet<MailDAO>{
       self.mail_dao.clone()
    }

    pub fn print_mail_scraper(&self){
        println!("email_adresses: {:?}
                 \nverbose: {}
                 \nconcurrency: {}",
                 self.mail_dao,
                 self.verbose,
                 self.concurrency)
    }

    fn mail_validation(&self, string: String) -> bool{
        if self.verbose{
            println!("mail_validation: {:?}", string);
        }
        let email_regex = Regex::new(r"^([a-z0-9_+]([a-z0-9_+.]*[a-z0-9_+])?)@([a-z0-9]+([\-\.]{1}[a-z0u9]+)*\.[a-z]{2,6})").unwrap();
        let email_regex2 = Regex::new(r"^([a-z0-9_+]([a-z0-9_+.]*[a-z0-9_+])?)\[at\]([a-z0-9]+([\-\.]{1}[a-z0u9]+)*\.[a-z]{2,6})").unwrap();
        email_regex.is_match(&string) || email_regex2.is_match(&string)
    }
}
